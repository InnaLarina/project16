﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Максимальный элемент списка: ");
            List<string> listStr = new List<string> { ("мама"), ("мыла"), ("панораму") };
            Console.WriteLine("{0}", String.Join("; ", listStr));
            String maxString = GetMax<string>(listStr, x => (x == null) ? 0 : x.Length);
            Console.WriteLine($"Это слово: {maxString}");
            Console.WriteLine();
            Console.WriteLine("Список файлов в каталоге. Прервались после первого найденного.");
            FileSearcher fs = new FileSearcher();
            fs.Search(@"D:\C#Education\На работе\Lesson1\Lesson1\ConsoleApp1\ConsoleApp1", "*.*");
            Console.ReadKey();
        }
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            T max = default(T);
            foreach (T item in e)
            {
                float m1 = getParametr(item);
                float m2 = getParametr(max);
                if (getParametr(item) > getParametr(max)) max = item;
            }
            return max;
        }

    }

}
